package region

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/shamansanchez/sc4go/dbpf"
)

// CityEntry is a single index entry
type CityEntry struct {
	Name      string
	Mayor     string
	TileX     uint32
	TileY     uint32
	Modified  uint32
	Path      string
	SHA256Sum string
}

// GetCities does stuff
func GetCities(path string) (map[string]*dbpf.DBPF, error) {
	cities := make(map[string]*dbpf.DBPF)

	paths, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}

	for _, c := range paths {
		name := c.Name()

		if filepath.Ext(name) == ".sc4" {
			cityPath := fmt.Sprintf("%s%c%s", path, filepath.Separator, name)
			city, err := dbpf.ReadDBPF(cityPath)
			if err != nil {
				return nil, err
			}

			cities[cityPath] = &city
		}
	}
	return cities, nil
}

func downloadCity(url string, path string) error {
	resp, err := http.Get(url)

	if err != nil {
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	c, err := dbpf.ReadRawDBPF(body)
	if err != nil {
		return err
	}

	r := dbpf.GetRegionData(c)

	log.Printf("Downloaded %s from the server!", r.Name)

	if err := ioutil.WriteFile(path, body, 0666); err != nil {
		return err
	}

	return nil
}

func downloadFile(url string, path string) error {
	resp, err := http.Get(url)

	if err != nil {
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	log.Printf("Downloaded %s!", url)

	if err := ioutil.WriteFile(path, body, 0666); err != nil {
		return err
	}

	return nil
}

//SyncRegion syncs the region with the server, downloading all cities
func SyncRegion(server string, regionPath string) error {
	indexURL := server + "/index"

	iniURL := server + "/static/region.ini"
	iniPath := fmt.Sprintf("%s%cregion.ini", regionPath, filepath.Separator)

	configURL := server + "/static/config.bmp"
	configPath := fmt.Sprintf("%s%cconfig.bmp", regionPath, filepath.Separator)

	// log.Println(indexURL, regionPath)

	if _, err := os.Stat(iniPath); os.IsNotExist(err) {
		downloadFile(iniURL, iniPath)
	} else {
		log.Println("region.ini exists")
	}

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		downloadFile(configURL, configPath)
	} else {
		log.Println("config.bmp exists")
	}

	index := make([]CityEntry, 0)

	resp, err := http.Get(indexURL)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, &index)
	if err != nil {
		return err
	}

	// log.Println(index)

	cities, err := GetCities(regionPath)
	if err != nil {
		return err
	}

	for _, entry := range index {
		// log.Println(entry.Path)

		found := false
		for path, city := range cities {
			info := dbpf.GetRegionData(*city)
			shasum := fmt.Sprintf("%x", city.SHA256Sum)

			if info.TileX == entry.TileX && info.TileY == entry.TileY {
				found = true
				// log.Printf("%s is at %s", info.Name, path)

				if entry.SHA256Sum == shasum {
					log.Printf("%s: up to date", entry.Name)
					break
				}

				if entry.Modified < city.Header.Modified {
					log.Printf("%s: server is older", entry.Name)
					// log.Printf("server: %s, local: %s", time.Unix(int64(entry.Modified), 0), time.Unix(int64(city.Header.Modified), 0))
					break
				}

				log.Printf("%s: updating...", entry.Name)
				downloadCity(server+entry.Path, path)
				break
			}
		}

		if !found {
			// log.Printf("City not found locally: %s!", entry.Name)
			downloadCity(server+entry.Path, fmt.Sprintf(
				"%s%c%d.%d.sc4", regionPath,
				filepath.Separator, entry.TileX, entry.TileY))

		}
	}

	return nil
}
