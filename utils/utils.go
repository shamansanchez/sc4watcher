package utils

import (
	"fmt"
	"log"
	"os"
)

// Fatal wraps log.Fatalf, pausing before exiting.
func Fatal(msg string, v ...interface{}) {
	log.Printf(msg, v...)
	log.Printf("Press enter...")
	fmt.Scanln()
	os.Exit(-1)
}
