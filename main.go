package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/google/uuid"
	"github.com/spf13/viper"
	"gitlab.com/shamansanchez/sc4go/dbpf"
	"gitlab.com/shamansanchez/sc4watcher/region"
	"gitlab.com/shamansanchez/sc4watcher/utils"
	"golang.org/x/sys/windows"
)

func consoleSetup() {
	var mode uint32

	err := windows.GetConsoleMode(windows.Stdin, &mode)
	if err != nil {
		utils.Fatal("Couldn't get console mode: %v", err)
	}

	mode &^= windows.ENABLE_QUICK_EDIT_MODE

	err = windows.SetConsoleMode(windows.Stdin, mode)
	if err != nil {
		utils.Fatal("Couldn't set console mode: %v", err)
	}
}

func main() {
	consoleSetup()
	log.Println("Starting up...")
	viper.SetConfigName("watcher")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		utils.Fatal("Could not load config file: %s", err)
	}
	log.Printf("Loaded config file: %s", viper.ConfigFileUsed())

	if !viper.IsSet("region") {
		utils.Fatal("'region' is not set in config file!")
	}

	if !viper.IsSet("client_id") {
		viper.Set("client_id", uuid.New())
		viper.WriteConfig()
	}

	regionName := viper.GetString("region")
	watcherPath := fmt.Sprintf("Regions\\%s", regionName)

	cityURL := fmt.Sprintf("%s/city", viper.GetString("server"))
	// indexURL := fmt.Sprintf("%s/index", viper.GetString("server"))

	if _, err := os.Stat(watcherPath); os.IsNotExist(err) {
		log.Printf("Region path '%s' does not exist!", watcherPath)
		os.Mkdir(watcherPath, os.FileMode(0755))
	}

	watcher, err := fsnotify.NewWatcher()
	defer watcher.Close()

	if err != nil {
		utils.Fatal("fsnotify failed: %s", err)
	}

	watcherDone := make(chan bool)

	go func() {
		log.Printf("Started watching %s", watcherPath)
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					watcherDone <- true
					return
				}
				if filepath.Ext(event.Name) == ".sc4" && (event.Op == fsnotify.Create || event.Op == fsnotify.Write) {
					// log.Println(event)
					rawBytes, _ := ioutil.ReadFile(event.Name)
					city, err := dbpf.ReadRawDBPF(rawBytes)
					if err != nil {
						log.Printf("Couldn't read DBPF %s, not uploading...", event.Name)
						continue
					}
					data := dbpf.GetRegionData(city)
					// log.Println("-----------")
					// log.Println(time.Unix(int64(city.Header.Created), 0))
					// log.Println(time.Unix(int64(city.Header.Modified), 0))
					// log.Printf("Name: \"%s\" Mayor: \"%s\"", data.Name, data.MayorName)
					// log.Printf("Updating GUID 0x%X %d.%d", data.GUID, data.TileX, data.TileY)
					// log.Println("-----------")
					// log.Println(indexURL)

					go func() {
						client := &http.Client{}

						req, _ := http.NewRequest("POST", cityURL, bytes.NewBuffer(rawBytes))

						req.Header.Set("X-Client-ID", viper.GetString("client_id"))
						log.Printf("Uploading %s", data.Name)
						client.Do(req)
					}()
				}
			}
		}
	}()

	region.SyncRegion(viper.GetString("server"), watcherPath)
	watcher.Add(watcherPath)

	ticker := time.NewTicker(60 * time.Second)
	tickerDone := make(chan bool)

	go func() {
		for {
			select {
			case <-tickerDone:
				return
			case t := <-ticker.C:
				log.Println("=============================")
				log.Println("syncing!", t)

				watcher.Remove(watcherPath)
				region.SyncRegion(viper.GetString("server"), watcherPath)
				watcher.Add(watcherPath)
				log.Println("=============================")
			}
		}
	}()
	<-watcherDone
}
