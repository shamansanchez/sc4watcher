import os
import requests

path = "<region path>"

headers = {"X-Client-ID": "client"}

for (subdir, dirs, files) in os.walk(path):
    for f in files:
        if f.endswith(".sc4"):
            city = subdir + os.sep + f

            with open(city, "rb") as c:
                raw = c.read()
                requests.post("https://<server_url>/city", data=raw, headers=headers)
